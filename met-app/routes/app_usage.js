var express = require('express');
var router = express.Router();

var fs = require('fs');
var dummyjson = require('dummy-json');
/* sample datas */
var appGuids = [
    "2785c9bf-b351-4503-824f-d87b1ffe028a",
    "2785c9bf-b351-4503-824f-d87b1ffe028b",
    "2785c9bf-b351-4503-824f-d87b1ffe028c",
    "2785c9bf-b351-4503-824f-d87b1ffe028d",
    "2785c9bf-b351-4503-824f-d87b1ffe028e",
    "2785c9bf-b351-4503-824f-d87b1ffe028f",
    "2785c9bf-b351-4503-824f-d87b1ffe028g",
    "2785c9bf-b351-4503-824f-d87b1ffe028h",
    "2785c9bf-b351-4503-824f-d87b1ffe028i",
    "2785c9bf-b351-4503-824f-d87b1ffe028j",
    "2785c9bf-b351-4503-824f-d87b1ffe028k",
    "2785c9bf-b351-4503-824f-d87b1ffe028l",
    "2785c9bf-b351-4503-824f-d87b1ffe028m",
    "2785c9bf-b351-4503-824f-d87b1ffe028n",
    "2785c9bf-b351-4503-824f-d87b1ffe028o",
    "2785c9bf-b351-4503-824f-d87b1ffe028p",
    "2785c9bf-b351-4503-824f-d87b1ffe028q",
    "2785c9bf-b351-4503-824f-d87b1ffe028r"
];
var appNames = [
    "app name a",
    "app name b",
    "app name c",
    "app name d",
    "app name e",
    "app name f",
    "app name g",
    "app name h",
    "app name i",
    "app name j",
    "app name k",
    "app name l",
    "app name m",
    "app name n",
    "app name o",
    "app name p",
    "app name q",
    "app name r"
];
var spaceGuids = [
    "ae1836fb-c214-4144-89ba-1ee05bf1d36a",
    "ae1836fb-c214-4144-89ba-1ee05bf1d36b",
    "ae1836fb-c214-4144-89ba-1ee05bf1d36c",
    "ae1836fb-c214-4144-89ba-1ee05bf1d36d",
    "ae1836fb-c214-4144-89ba-1ee05bf1d36e",
    "ae1836fb-c214-4144-89ba-1ee05bf1d36f",
    "ae1836fb-c214-4144-89ba-1ee05bf1d36g",
    "ae1836fb-c214-4144-89ba-1ee05bf1d36h",
    "ae1836fb-c214-4144-89ba-1ee05bf1d36i",
    "ae1836fb-c214-4144-89ba-1ee05bf1d36j",
    "ae1836fb-c214-4144-89ba-1ee05bf1d36k",
    "ae1836fb-c214-4144-89ba-1ee05bf1d36l",
    "ae1836fb-c214-4144-89ba-1ee05bf1d36m",
    "ae1836fb-c214-4144-89ba-1ee05bf1d36n",
    "ae1836fb-c214-4144-89ba-1ee05bf1d36o",
    "ae1836fb-c214-4144-89ba-1ee05bf1d36a",
    "ae1836fb-c214-4144-89ba-1ee05bf1d36q",
    "ae1836fb-c214-4144-89ba-1ee05bf1d36q"
];

var orgGuids = [
    "a7700ecd-f6bd-4e09-8b2e-2897312bbf6a",
    "a7700ecd-f6bd-4e09-8b2e-2897312bbf6b",
    "a7700ecd-f6bd-4e09-8b2e-2897312bbf6c",
    "a7700ecd-f6bd-4e09-8b2e-2897312bbf6d",
    "a7700ecd-f6bd-4e09-8b2e-2897312bbf6e",
    "a7700ecd-f6bd-4e09-8b2e-2897312bbf6f",
    "a7700ecd-f6bd-4e09-8b2e-2897312bbf6g",
    "a7700ecd-f6bd-4e09-8b2e-2897312bbf6h",
    "a7700ecd-f6bd-4e09-8b2e-2897312bbf6i",
    "a7700ecd-f6bd-4e09-8b2e-2897312bbf6j",
    "a7700ecd-f6bd-4e09-8b2e-2897312bbf6k",
    "a7700ecd-f6bd-4e09-8b2e-2897312bbf6l",
    "a7700ecd-f6bd-4e09-8b2e-2897312bbf6m",
    "a7700ecd-f6bd-4e09-8b2e-2897312bbf6n",
    "a7700ecd-f6bd-4e09-8b2e-2897312bbf6o",
    "a7700ecd-f6bd-4e09-8b2e-2897312bbf6a",
    "a7700ecd-f6bd-4e09-8b2e-2897312bbf6a",
    "a7700ecd-f6bd-4e09-8b2e-2897312bbf6a"
];

var template = fs.readFileSync('data/template_app_usage.hbs', {encoding: 'utf8'});


var helpers = {
    appGuid: function(options) {
        return appGuids[options.data.index];
    },
    appName: function(options) {
        return appNames[options.data.index];
    },
    spaceGuid: function(options) {
        return spaceGuids[options.data.index];
    },
    orgGuid: function(options) {
        return orgGuids[options.data.index];
    }
};

/* GET app usage. */
router.get('/', function(req, res) {

    var result = dummyjson.parse(template, {helpers: helpers});

    res.send(result);
});

module.exports = router;
