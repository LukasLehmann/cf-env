var express = require('express');
var router = express.Router();

var fs = require('fs');
var dummyjson = require('dummy-json');
/* sample datas */
var serviceGuids = [
    "1085c9bf-b351-4503-824f-d87b1ffe028a",
    "1085c9bf-b351-4503-824f-d87b1ffe028b",
    "1085c9bf-b351-4503-824f-d87b1ffe028c",
    "1085c9bf-b351-4503-824f-d87b1ffe028d",
    "1085c9bf-b351-4503-824f-d87b1ffe028e",
    "1085c9bf-b351-4503-824f-d87b1ffe028f",
    "1085c9bf-b351-4503-824f-d87b1ffe028g",
    "1085c9bf-b351-4503-824f-d87b1ffe028h",
    "1085c9bf-b351-4503-824f-d87b1ffe028i",
    "1085c9bf-b351-4503-824f-d87b1ffe028j",
    "1085c9bf-b351-4503-824f-d87b1ffe028k",
    "1085c9bf-b351-4503-824f-d87b1ffe028l",
    "1085c9bf-b351-4503-824f-d87b1ffe028m",
    "1085c9bf-b351-4503-824f-d87b1ffe028n",
    "1085c9bf-b351-4503-824f-d87b1ffe028o",
    "1085c9bf-b351-4503-824f-d87b1ffe028p",
    "1085c9bf-b351-4503-824f-d87b1ffe028q",
    "1085c9bf-b351-4503-824f-d87b1ffe028r"
];
var serviceNames = [
    "services name a",
    "services name b",
    "services name c",
    "services name d",
    "services name e",
    "services name f",
    "services name g",
    "services name h",
    "services name i",
    "services name j",
    "services name k",
    "services name l",
    "services name m",
    "services name n",
    "services name o",
    "services name p",
    "services name q",
    "services name r"
];
var spaceGuids = [
    "ae1836fb-c214-4144-89ba-1ee05bf1d36a",
    "ae1836fb-c214-4144-89ba-1ee05bf1d36b",
    "ae1836fb-c214-4144-89ba-1ee05bf1d36c",
    "ae1836fb-c214-4144-89ba-1ee05bf1d36d",
    "ae1836fb-c214-4144-89ba-1ee05bf1d36e",
    "ae1836fb-c214-4144-89ba-1ee05bf1d36f",
    "ae1836fb-c214-4144-89ba-1ee05bf1d36g",
    "ae1836fb-c214-4144-89ba-1ee05bf1d36h",
    "ae1836fb-c214-4144-89ba-1ee05bf1d36i",
    "ae1836fb-c214-4144-89ba-1ee05bf1d36j",
    "ae1836fb-c214-4144-89ba-1ee05bf1d36k",
    "ae1836fb-c214-4144-89ba-1ee05bf1d36l",
    "ae1836fb-c214-4144-89ba-1ee05bf1d36m",
    "ae1836fb-c214-4144-89ba-1ee05bf1d36n",
    "ae1836fb-c214-4144-89ba-1ee05bf1d36o",
    "ae1836fb-c214-4144-89ba-1ee05bf1d36a",
    "ae1836fb-c214-4144-89ba-1ee05bf1d36q",
    "ae1836fb-c214-4144-89ba-1ee05bf1d36q"
];

var orgGuids = [
    "a7700ecd-f6bd-4e09-8b2e-2897312bbf6a",
    "a7700ecd-f6bd-4e09-8b2e-2897312bbf6b",
    "a7700ecd-f6bd-4e09-8b2e-2897312bbf6c",
    "a7700ecd-f6bd-4e09-8b2e-2897312bbf6d",
    "a7700ecd-f6bd-4e09-8b2e-2897312bbf6e",
    "a7700ecd-f6bd-4e09-8b2e-2897312bbf6f",
    "a7700ecd-f6bd-4e09-8b2e-2897312bbf6g",
    "a7700ecd-f6bd-4e09-8b2e-2897312bbf6h",
    "a7700ecd-f6bd-4e09-8b2e-2897312bbf6i",
    "a7700ecd-f6bd-4e09-8b2e-2897312bbf6j",
    "a7700ecd-f6bd-4e09-8b2e-2897312bbf6k",
    "a7700ecd-f6bd-4e09-8b2e-2897312bbf6l",
    "a7700ecd-f6bd-4e09-8b2e-2897312bbf6m",
    "a7700ecd-f6bd-4e09-8b2e-2897312bbf6n",
    "a7700ecd-f6bd-4e09-8b2e-2897312bbf6o",
    "a7700ecd-f6bd-4e09-8b2e-2897312bbf6a",
    "a7700ecd-f6bd-4e09-8b2e-2897312bbf6a",
    "a7700ecd-f6bd-4e09-8b2e-2897312bbf6a"
];
var usageUnits = [
    "GB",
    "GB",
    "MB",
    "GB",
    "KB",
    "GB",
    "KB/S",
    "GB",
    "GB",
    "MB/S",
    "GB",
    "pieces",
    "GB",
    "GB",
    "GB",
    "per 1000 sms",
    "km/h",
    "kalc"
];
var template = fs.readFileSync('data/template_service_usage.hbs', {encoding: 'utf8'});


var helpers = {
    serviceGuid: function(options) {
        return serviceGuids[options.data.index];
    },
    serviceName: function(options) {
        return serviceNames[options.data.index];
    },
    spaceGuid: function(options) {
        return spaceGuids[options.data.index];
    },
    orgGuid: function(options) {
        return orgGuids[options.data.index];
    },
    usageUnit: function(options) {
        return usageUnits[options.data.index];
    }
};

/* GET services usage. */
router.get('/', function(req, res) {

    var result = dummyjson.parse(template, {helpers: helpers});

    res.send(result);
});

module.exports = router;
